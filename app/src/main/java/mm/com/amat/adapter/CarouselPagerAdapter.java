package mm.com.amat.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import mm.com.amat.ui.fragment.FragmentCarousel;


public class CarouselPagerAdapter extends FragmentPagerAdapter {
    public CarouselPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return FragmentCarousel.newInstance(position+1);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
