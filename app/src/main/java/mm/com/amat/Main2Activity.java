package mm.com.amat;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import in.mrasif.libs.easyqr.EasyQR;
import in.mrasif.libs.easyqr.QRScanner;
import mm.com.amat.adapter.CarouselPagerAdapter;

import mm.com.amat.utils.Utils;

import android.animation.ArgbEvaluator;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;

import android.widget.Button;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    /**
     * The {@link androidx.viewpager.widget.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * androidx.fragment.app.FragmentStatePagerAdapter.
     */
    private CarouselPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Button startBtn;
    private Utils utils;
    int color1,color2,color3;
    int[] colorList;
    ArgbEvaluator evaluator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        color1 = ContextCompat.getColor(this, R.color.firstWindow);
        color2 = ContextCompat.getColor(this, R.color.secondWindow);
        color3 = ContextCompat.getColor(this, R.color.thirdWindow);

        colorList = new int[]{color1, color2, color3};
        evaluator = new ArgbEvaluator();
        mSectionsPagerAdapter = new CarouselPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        startBtn = findViewById(R.id.startBtn);

        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(this);
//        startBtn.setOnClickListener(v->{
//            if (Utils.isNetworkConnect(this)){
//                Utils.requestPermission(this);
//
//                Intent intent=new Intent(this, QRScanner.class);
//                intent.putExtra(EasyQR.CAMERA_BORDER,100);
//                intent.putExtra(EasyQR.CAMERA_BORDER_COLOR,"#C1000000");
//                intent.putExtra(EasyQR.IS_SCAN_BAR,true);
//                intent.putExtra(EasyQR.IS_BEEP,true);
//                intent.putExtra(EasyQR.BEEP_RESOURCE_ID,R.raw.beep);
//                startActivityForResult(intent, EasyQR.QR_SCANNER_REQUEST);
//            }
//
//            else Toast.makeText(this,"werwerwer",Toast.LENGTH_SHORT).show();
//        });

    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode){
//            case EasyQR.QR_SCANNER_REQUEST: {
//                if (resultCode==RESULT_OK){
//                    startBtn.setText(data.getStringExtra(EasyQR.DATA));
//                }
//            } break;
//        }
//    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 2 ? position : position + 1]);
        mViewPager.setBackgroundColor(colorUpdate);
    }

    @Override
    public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mViewPager.setBackgroundColor(color1);
                        break;
                    case 1:
                        mViewPager.setBackgroundColor(color2);
                        break;
                    case 2:
                        mViewPager.setBackgroundColor(color3);
                        break;
                }
                startBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
