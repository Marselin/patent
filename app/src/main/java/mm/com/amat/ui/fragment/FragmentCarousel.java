package mm.com.amat.ui.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mm.com.amat.R;


public class FragmentCarousel extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
//    private static final String ARG_SECTION_COLOR = "section_color";

    public FragmentCarousel() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentCarousel newInstance(int sectionNumber) {
        FragmentCarousel fragment = new FragmentCarousel();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        args.putInt(ARG_SECTION_COLOR,color);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_carousel, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//         rootView.setBackgroundColor(getArguments().getInt(ARG_SECTION_COLOR));

        return rootView;
    }

}
